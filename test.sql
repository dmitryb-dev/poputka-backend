-- select *
--     from ride__locations as l1
--     left join ride__locations as l2 on l1.ride_id = l2.ride_id
--     order by (l1.point <-> '(250.0, 40.0)') - (l2.point <-> '(2.0, 2.0)')

explain
    select
            min(point <-> '(250.0, 40.0)') + min(point <-> '(2.5, 2.0)') as best,
            ride_id
    from ride__locations group by ride_id
    order by best
    limit 20 offset 500;

explain select ride_id, point <-> '(250.0, 40.0)' as dist
    from ride__locations
    order by dist
    limit 20 offset 500;

explain select ride_id, min(point <-> '(250.0, 40.0)') as dist
        from ride__locations group by ride_id
        order by dist
        limit 20 offset 500;

explain select ride_id, min(dist) from (select ride_id, point <-> '(250.0, 40.0)' as dist
        from ride__locations
        order by dist limit 20 offset 500) as l group by ride_id;


explain
    with
        nearest_locations_from as (
            select ride_id, point <-> '(250.0, 40.0)' as distance_from
                from ride__locations
            order by distance_from
        ),
        nearest_location_from_with_type as (
            select *
                from nearest_locations_from
                left join rides on nearest_locations_from.ride_id = rides.id -- change to inner join
                where type is null
                limit 4 * 150
        ),
        nearest_rides_from as (
            select ride_id, min(distance_from) as ride_distance_from
                from nearest_location_from_with_type
            group by nearest_location_from_with_type.ride_id
            order by ride_distance_from
            limit 20
        ),
        rides_and_distances as (
            select nearest_rides_from.ride_id,
                   nearest_rides_from.ride_distance_from,
                   min(ride__locations.point <-> '(2.5, 2.0)') as ride_distance_to
                from nearest_rides_from
                left join ride__locations on nearest_rides_from.ride_id = ride__locations.ride_id
            group by (nearest_rides_from.ride_id, nearest_rides_from.ride_distance_from)
        ),
        best_rides as (
            select ride_id from rides_and_distances order by (ride_distance_from + ride_distance_to)
        )

    select * from best_rides;

create index point_index on ride__locations using spgist(point);

insert into ride__locations(ride_id, point)

select
    random() * 10000,
    point(random() * 10, random() * 10)
from generate_series(1, 100000) id;


create materialized view locations_search_table as
    select l.id as lid, r.id as rid, point, date, type
    from ride__locations as l left join rides as r on l.ride_id = r.id;

refresh materialized view locations_search_table;


create index date_gist on locations_search_table using gist(date, point);

