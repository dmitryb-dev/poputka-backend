FROM openjdk:14.0.1-jdk AS builder

COPY ./ /poputka
WORKDIR /poputka
RUN ./gradlew build


FROM openjdk:14.0.1-jdk

WORKDIR /poputka
COPY --from=builder /poputka/build/libs/poputka-1.0.jar /app.jar

ENTRYPOINT [ "java", "-jar", "/app.jar" ]