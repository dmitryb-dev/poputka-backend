CREATE TABLE ride__search(
                             location_id bigserial primary key,
                             ride_id bigint,
                             status varchar(64),
                             type varchar(64),
                             datetime_start timestamp,
                             point point
);

CREATE EXTENSION btree_gist;

create index ride__search__index on ride__search using gist(status, type, datetime_start, point);
create index test_index on ride__search using gist(point);
create index test_index3 on ride__search using gist(ride_id);

insert into ride__search(ride_id, status, type, datetime_start, point)

select
        random() * 10000,
        'open',
        'driver',
        current_date,
        point(random() * 10, random() * 10)
from generate_series(1, 100000) id;

explain select ride_id from ride__search
        where status = 'open' and type = 'driver' and datetime_start = current_date
        order by point <-> '(1, 1)';

explain select ride_id, point from ride__search
        where status = 'open' and type = 'driver' and datetime_start = current_date
        order by point <-> '(1, 1)' limit 20;

explain analyse select ride_id, point from ride__search
        where datetime_start >= current_date and status = 'open' and type = 'driver'
        order by point <-> '(1, 1)';

explain analyse select ride_id, min(point <-> '(1, 1)') as dist from ride__search
                where datetime_start >= current_date and status = 'open' and type = 'driver'
                group by ride_id
                order by dist
                limit 20;

explain analyse select ride_id, point from ride__search
                where datetime_start >= current_date and status = 'open' and type = 'driver'
                limit 20;

select l.id from rides as r inner join ride__locations l on r.id = l.ride_id;