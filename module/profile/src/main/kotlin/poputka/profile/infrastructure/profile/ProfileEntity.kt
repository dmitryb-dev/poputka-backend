package poputka.profile.infrastructure.profile

import poputka.profile.domain.Profile
import javax.persistence.*

@Table(name = "profile")
@Entity
data class ProfileEntity(
        @Column(name = "user_id") @Id val userId: Long,
        @Column(name = "name") val name: String? = null,
        @Column(name = "surname") val surname: String? = null,
        @Column(name = "image_url") val imageUrl: String? = null,
        @Column(name = "phone") val phone: String? = null,
        @Column(name = "email") val email: String? = null,
        @Column(name = "city") val city: String? = null,
        @Column(name = "country") val country: String? = null,
        @Column(name = "gender") @Enumerated(EnumType.STRING) val gender: Gender? = null
) {
    fun toDomain(): Profile = Profile(
            userId,
            name,
            surname,
            imageUrl,
            phone,
            email,
            city,
            country,
            when (gender) {
                Gender.MALE -> poputka.profile.domain.Gender.MALE
                Gender.FEMALE -> poputka.profile.domain.Gender.FEMALE
                null -> null
            }
    )
}

enum class Gender {
    MALE, FEMALE
}