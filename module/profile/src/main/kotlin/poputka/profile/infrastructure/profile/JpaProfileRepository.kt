package poputka.profile.infrastructure.profile

import org.springframework.data.jpa.repository.JpaRepository

interface JpaProfileRepository : JpaRepository<ProfileEntity, Long>