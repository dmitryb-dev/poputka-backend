package poputka.profile.infrastructure.profile

import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import poputka.profile.domain.Profile
import poputka.profile.service.ProfileStore

@Repository
class SpringProfileStore(
        private val profileRepository: JpaProfileRepository
) : ProfileStore {

    @Transactional
    override fun saveProfile(profile: Profile) {
        profileRepository.save(ProfileEntity(
                profile.userId,
                profile.name,
                profile.surname,
                profile.imageUrl,
                profile.phone,
                profile.email,
                profile.city,
                profile.country,
                when (profile.gender) {
                    poputka.profile.domain.Gender.MALE -> Gender.MALE
                    poputka.profile.domain.Gender.FEMALE -> Gender.FEMALE
                    else -> null
                }
        ))
    }

    override fun getProfile(userId: Long): Profile? {
        val profile = profileRepository.findById(userId).let {
            if (it.isEmpty) return null
            it.get()
        }

        return Profile(
                userId,
                profile.name,
                profile.surname,
                profile.imageUrl,
                profile.phone,
                profile.email,
                profile.city,
                profile.country,
                when (profile.gender) {
                    Gender.MALE -> poputka.profile.domain.Gender.MALE
                    Gender.FEMALE -> poputka.profile.domain.Gender.FEMALE
                    else -> null
                }
        )
    }
}