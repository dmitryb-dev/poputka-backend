package poputka.profile.domain

class Profile(
        val userId: Long,
        val name: String?,
        val surname: String?,
        val imageUrl: String?,
        val phone: String?,
        val email: String?,
        val city: String?,
        val country: String?,
        val gender: Gender?
)

enum class Gender {
    MALE, FEMALE
}