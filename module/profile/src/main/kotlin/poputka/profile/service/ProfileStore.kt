package poputka.profile.service

import poputka.profile.domain.Profile

interface ProfileStore {
    fun saveProfile(profile: Profile)
    fun getProfile(userId: Long): Profile?
}