plugins {
	id("org.springframework.boot")
	id("io.spring.dependency-management")
	kotlin("jvm")
	kotlin("plugin.spring")
	kotlin("plugin.jpa")
}

tasks.getByName("bootJar") {
	enabled = false
}
tasks.getByName("jar") {
	enabled = true
}

repositories {
	mavenCentral()
}

dependencies {
	implementation(project(":module:common"))
	implementation(project(":module:profile"))

	implementation("org.springframework.boot:spring-boot-starter")
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("com.auth0:java-jwt:3.8.3")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("com.google.firebase:firebase-admin:6.12.2")
	implementation("org.postgresql:postgresql")
}

tasks.withType<Test> {
	useJUnitPlatform()
}
