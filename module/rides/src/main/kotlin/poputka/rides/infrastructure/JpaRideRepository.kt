package poputka.rides.infrastructure

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface JpaRideRepository : JpaRepository<RideEntity, Long> {
    fun findByUserId(userId: Long, pageable: Pageable): Page<RideEntity>
}