package poputka.rides.infrastructure.util

import org.hibernate.engine.spi.SharedSessionContractImplementor
import org.hibernate.type.CustomType
import org.hibernate.usertype.UserType
import org.postgresql.geometric.PGpoint
import java.sql.PreparedStatement
import java.sql.ResultSet

class PGpointType : UserType {
    override fun sqlTypes() = intArrayOf(java.sql.Types.OTHER)

    override fun returnedClass(): Class<*> = PGpoint::class.java

    override fun nullSafeGet(rs: ResultSet, names: Array<out String>, session: SharedSessionContractImplementor?, owner: Any?): Any {
        return rs.getObject(names[0]) as PGpoint
    }

    override fun nullSafeSet(st: PreparedStatement, value: Any?, index: Int, session: SharedSessionContractImplementor?) {
        if (value == null) {
            st.setNull(index, java.sql.Types.OTHER)
        } else {
            st.setObject(index, value)
        }
    }

    override fun deepCopy(o: Any?) = (o as PGpoint).clone()

    override fun equals(o: Any?, o1: Any?) = o == o1

    override fun isMutable() = false

    override fun hashCode(o: Any) = o.hashCode()

    override fun disassemble(o: Any): java.io.Serializable = o as java.io.Serializable

    override fun assemble(cached: java.io.Serializable, owner: Any) = cached

    override fun replace(original: Any, target: Any, owner: Any) = original

    companion object {
        val type = CustomType(PGpointType())
    }
}
