package poputka.rides.infrastructure

import org.hibernate.annotations.*
import org.postgresql.geometric.PGpoint
import poputka.profile.infrastructure.profile.ProfileEntity
import poputka.rides.domain.DepartureTime
import poputka.rides.domain.Location
import poputka.rides.domain.Ride
import poputka.rides.domain.Seats
import poputka.rides.infrastructure.util.PGpointType
import java.time.LocalDate
import java.time.LocalTime
import javax.persistence.*
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.Table

@Table(name = "rides")
@Entity
data class RideEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        val id: Long,

        @Column(name = "seats_total") var totalSeats: Int,
        @Column(name = "seats_taken") var takenSeats: Int,

        @Column(name = "date") var date: LocalDate,
        @Column(name = "time_start") var timeStart: LocalTime,
        @Column(name = "time_end") var timeEnd: LocalTime,

        @Column(name = "type") @Enumerated(EnumType.STRING) var type: RideType,
        @Column(name = "status") @Enumerated(EnumType.STRING) var status: RideStatus,
        @Column(name = "description", length = 2048) var description: String,

        @OneToMany(mappedBy = "ride", cascade = [ CascadeType.ALL ], fetch = FetchType.EAGER, orphanRemoval = true)
        @Fetch(value = FetchMode.JOIN)
        val locations: MutableCollection<LocationEntity>,

        @Column(name = "user_id") val userId: Long,

        @OneToOne(fetch = FetchType.EAGER, optional = true)
        @JoinColumn(name = "user_id", insertable = false, updatable = false, nullable = true)
        @Fetch(value = FetchMode.JOIN)
        @NotFound(action = NotFoundAction.IGNORE)
        val user: ProfileEntity?
) {
    fun toDomain(): Ride = Ride(
            this.id,
            (this.user ?: ProfileEntity(this.userId)).toDomain(),
            DepartureTime(this.date, this.timeStart, this.timeEnd),
            this.locations.map(LocationEntity::toDomain),
            Seats(this.totalSeats, this.takenSeats),
            this.type.toDomain(),
            this.status.toDomain(),
            this.description
    )
}

@Table(name = "ride__locations")
@Entity
@TypeDefs(TypeDef(name = "point", typeClass = PGpointType::class))
data class LocationEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        val id: Long,

        @Column(name = "point", columnDefinition = "point")
        @Type(type = "point")
        val point: PGpoint,

        @Column(name = "description")
        val description: String?,

        @ManyToOne
        @JoinColumn(name = "ride_id")
        val ride: RideEntity
) {
    fun toDomain() = Location(point.x, point.y, description)

    companion object {
        fun fromDomain(location: Location, ride: RideEntity) =
                LocationEntity(
                        0,
                        PGpoint(location.latitude, location.longitude),
                        location.description,
                        ride
                )
    }
}

enum class RideType {
    DRIVER, PASSENGER;

    fun toDomain() = when (this) {
        DRIVER -> poputka.rides.domain.RideType.DRIVER
        PASSENGER -> poputka.rides.domain.RideType.PASSENGER
    }

    companion object {
        fun fromDomain(type: poputka.rides.domain.RideType) = when (type) {
            poputka.rides.domain.RideType.DRIVER -> DRIVER
            poputka.rides.domain.RideType.PASSENGER -> PASSENGER
        }
    }
}
enum class RideStatus {
    OPEN, CLOSED, FINISHED;

    fun toDomain() = when (this) {
        OPEN -> poputka.rides.domain.RideStatus.OPEN
        CLOSED -> poputka.rides.domain.RideStatus.CLOSED
        FINISHED -> poputka.rides.domain.RideStatus.FINISHED
    }

    companion object {
        fun fromDomain(status: poputka.rides.domain.RideStatus) = when (status) {
            poputka.rides.domain.RideStatus.OPEN -> OPEN
            poputka.rides.domain.RideStatus.CLOSED -> CLOSED
            poputka.rides.domain.RideStatus.FINISHED -> FINISHED
        }
    }
}