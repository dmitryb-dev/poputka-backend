package poputka.rides.infrastructure

import org.hibernate.Criteria
import org.hibernate.Session
import org.hibernate.type.CharacterArrayType
import org.hibernate.type.StringType
import org.postgresql.geometric.PGpoint
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import poputka.common.exception.AccessDeniedException
import poputka.profile.infrastructure.profile.ProfileEntity
import poputka.rides.domain.*
import poputka.rides.domain.RideType
import poputka.rides.infrastructure.util.PGpointType
import poputka.rides.infrastructure.util.SearchQueryProvider
import poputka.rides.service.RideId
import poputka.rides.service.RidesManager
import poputka.rides.service.RidesStore
import java.time.LocalDateTime
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Repository
class SpringRidesStore(
        private val rideRepository: JpaRideRepository,
        @PersistenceContext private val entityManager: EntityManager,
        @Value("\${app.maxRidesToFetch}") private val maxRidesToFetch: Int,
        @Value("\${app.maxLocationsPerRide}") private val maxLocationsPerRide: Int
) : RidesStore {

    @Transactional
    override fun findRides(
            from: Location?,
            to: Location?,
            radiusKm: Double?,
            type: RideType?,
            fromDatetime: LocalDateTime,
            pageable: Pageable
    ): Collection<Ride> {
        val parsedType = when (type) {
            RideType.DRIVER -> poputka.rides.infrastructure.RideType.DRIVER
            RideType.PASSENGER -> poputka.rides.infrastructure.RideType.PASSENGER
            null -> null
        }

        val session = entityManager.unwrap(Session::class.java) as Session

        val query = if (from != null || to != null) {
            val queryWithLocation = if (from != null && to != null) {
                session.createNativeQuery(SearchQueryProvider.fromAndToLocations)
                        .setParameter("to", PGpoint(to.latitude, to.longitude), PGpointType.type)
            } else {
                session.createNativeQuery(SearchQueryProvider.fromLocation)
            }

            val fromActual = from ?: to!!
            val radiusMiles = (radiusKm ?: 100000.0).div(Companion.MILE_KM_RATIO)

            queryWithLocation
                    .setParameter("radius", radiusMiles)
                    .setParameter("max_locations_per_ride", maxLocationsPerRide)
                    .setParameter("max_rides_to_fetch", maxRidesToFetch)
                    .setParameter("from", PGpoint(fromActual.latitude, fromActual.longitude), PGpointType.type)

        } else {
            session.createNativeQuery(SearchQueryProvider.noLocations)
        }

        query
                .setParameter("status", RideStatus.OPEN.name)
                .setParameter("type", parsedType?.name, StringType())
                .setParameter("current_datetime", fromDatetime)
                .setParameter("limit", pageable.pageSize)
                .setParameter("offset", pageable.offset)

        query
                .addEntity("location", LocationEntity::class.java)
                .addEntity("profile", ProfileEntity::class.java)
                .addJoin("ride", "location.ride")
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)


        return query.resultList
                .map { entity -> (entity as RideEntity).toDomain()}
    }

    override fun findRidesByUser(userId: Long, pageable: Pageable): Collection<Ride> {
        return rideRepository.findByUserId(userId, pageable)
                .map { it.toDomain() }
                .toList()
    }

    override fun findRideById(rideId: Long): Ride {
        return rideRepository.findById(rideId).get()
                .toDomain()
    }

    @Transactional
    override fun addRide(userId: Long, ride: RidesManager.RideValue): RideId {
        val entityToSave = RideEntity(
                0,
                ride.seats.total,
                ride.seats.taken,
                ride.departureTime.date,
                ride.departureTime.timeStart,
                ride.departureTime.timeEnd,
                poputka.rides.infrastructure.RideType.fromDomain(ride.type),
                RideStatus.fromDomain(ride.status),
                ride.description,
                ArrayList(),
                userId,
                null
        )

        entityToSave.locations.addAll(
                ride.locations.map { LocationEntity.fromDomain(it, entityToSave) }
        )

        rideRepository.saveAndFlush(entityToSave)

        return entityToSave.id
    }

    @Transactional
    override fun updateRide(userId: Long, rideId: Long, ride: RidesManager.RideValue) {
        val entityToUpdate = rideRepository.findById(rideId).get()

        if (entityToUpdate.userId != userId) {
            throw AccessDeniedException()
        }

        entityToUpdate.totalSeats = ride.seats.total
        entityToUpdate.takenSeats = ride.seats.taken
        entityToUpdate.type = poputka.rides.infrastructure.RideType.fromDomain(ride.type)
        entityToUpdate.status = RideStatus.fromDomain(ride.status)
        entityToUpdate.date = ride.departureTime.date
        entityToUpdate.timeStart = ride.departureTime.timeStart
        entityToUpdate.timeEnd = ride.departureTime.timeEnd

        entityToUpdate.locations.clear()

        rideRepository.saveAndFlush(entityToUpdate)

        entityToUpdate.locations.addAll(ride.locations.map { LocationEntity.fromDomain(it, entityToUpdate) })
    }

    companion object {
        private const val MILE_KM_RATIO = 1.609344
    }
}