package poputka.rides.infrastructure.util

import org.springframework.util.ResourceUtils

object SearchQueryProvider {
    private val queryTemplate = ResourceUtils.getURL("classpath:rides_search.sql").readText()
    private val fields = "{ride.*}, {location.*}, {profile.*}"

    val noLocations = queryTemplate
            .replace(":sub_query_placeholder", "all_rides")
            .replace(":max_locations_per_ride", "0")
            .replace(":max_rides_to_fetch", "0")
            .replace(":from", "'(0, 0)'")
            .replace(":to", "'(0, 0)'")
            .replace(":radius", "0")
            .replace(":fields", fields)

    val fromLocation = queryTemplate
            .replace(":sub_query_placeholder", "nearest_rides_for_location_from")
            .replace(":to", "'(0, 0)'")
            .replace(":fields", fields)

    val fromAndToLocations = queryTemplate
            .replace(":sub_query_placeholder", "nearest_rides_for_location_from_and_to")
            .replace(":fields", fields)
}