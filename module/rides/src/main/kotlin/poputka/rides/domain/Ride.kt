
package poputka.rides.domain

import poputka.profile.domain.Profile
import java.time.LocalDate
import java.time.LocalTime

class Ride(
        val id: Long,
        val user: Profile,
        val departureTime: DepartureTime,
        val locations: Collection<Location>,
        val seats: Seats,
        val type: RideType,
        val status: RideStatus,
        val description: String
)

class Location(val latitude: Double, val longitude: Double, val description: String?)
class Seats(val total: Int, val taken: Int)

class DepartureTime(val date: LocalDate, val timeStart: LocalTime, val timeEnd: LocalTime)

enum class RideType { DRIVER, PASSENGER }
enum class RideStatus { OPEN, CLOSED, FINISHED }