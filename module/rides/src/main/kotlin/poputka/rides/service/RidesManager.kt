package poputka.rides.service

import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import poputka.common.exception.IllegalApiUsageException
import poputka.common.exception.LocalizedException
import poputka.rides.domain.*
import java.time.LocalDateTime

typealias RideId = Long

@Service
class RidesManager(
        @Value("\${app.maxLocationsPerRide}") private val maxLocationsPerRide: Int,
        private val ridesStore: RidesStore
) {
    fun findRides(
            from: Location?,
            to: Location?,
            radiusKm: Double?,
            type: RideType?,
            fromDatetime: LocalDateTime,
            pageable: Pageable
    ): Collection<Ride> {
        return ridesStore.findRides(from, to, radiusKm, type, fromDatetime, pageable)
    }

    fun findUserRides(userId: Long, pageable: Pageable): Collection<Ride> {
        return ridesStore.findRidesByUser(userId, pageable)
    }

    fun findById(rideId: Long): Ride {
        return ridesStore.findRideById(rideId)
    }

    fun addRide(userId: Long, ride: RideValue): RideId {
        if (ride.locations.size > maxLocationsPerRide) {
            throw LocationsNumberExceededException()
        }

        return ridesStore.addRide(userId, ride)
    }

    fun updateRide(userId: Long, rideId: Long, ride: RideValue) {
        if (ride.locations.size > maxLocationsPerRide) {
            throw LocationsNumberExceededException()
        }

        ridesStore.updateRide(userId, rideId, ride)
    }

    class RideValue(
            val departureTime: DepartureTime,
            val locations: Collection<Location>,
            val seats: Seats,
            val type: RideType,
            val status: RideStatus,
            val description: String
    )

    inner class LocationsNumberExceededException : LocalizedException(
            "It's not allowed to set more then $maxLocationsPerRide locations to a ride",
            "too_many_locations",
            maxLocationsPerRide
    )
}