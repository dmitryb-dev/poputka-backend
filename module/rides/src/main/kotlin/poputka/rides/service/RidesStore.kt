package poputka.rides.service

import org.springframework.data.domain.Pageable
import poputka.rides.domain.Location
import poputka.rides.domain.Ride
import poputka.rides.domain.RideType
import java.time.LocalDateTime

interface RidesStore {
    fun findRides(
            from: Location?,
            to: Location?,
            radiusKm: Double?,
            type: RideType?,
            fromDatetime: LocalDateTime,
            pageable: Pageable
    ): Collection<Ride>

    fun findRidesByUser(userId: Long, pageable: Pageable): Collection<Ride>
    fun findRideById(rideId: Long): Ride

    fun addRide(userId: Long, ride: RidesManager.RideValue): RideId
    fun updateRide(userId: Long, rideId: Long, ride: RidesManager.RideValue)
}