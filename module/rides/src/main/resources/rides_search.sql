WITH
    _nearest_locations_to_location_from AS (
        SELECT *, point <-> :from AS location_distance_from
            FROM ride__search
        WHERE (:status is null OR :status = status)
            AND (:type is null OR :type = type)
            AND (:from <@> point < :radius)
            AND datetime_start > :current_datetime
        ORDER BY location_distance_from
        LIMIT :max_locations_per_ride * :max_rides_to_fetch
    ),
    nearest_rides_for_location_from AS (
        SELECT
               ride_id,
               MIN(location_distance_from) AS ride_distance_from,
               MIN(location_distance_from) AS distance
            FROM _nearest_locations_to_location_from
        GROUP BY ride_id
        ORDER BY ride_distance_from
        LIMIT :limit OFFSET :offset
    ),
    _nearest_rides_for_location_from_plus_ride_distance_to AS (
        SELECT
               r.ride_id,
               r.ride_distance_from,
               MIN(:to <-> s.point) AS ride_distance_to
            FROM nearest_rides_for_location_from AS r INNER JOIN ride__search AS s ON r.ride_id = s.ride_id
        GROUP BY (r.ride_id, r.ride_distance_from)
    ),
    nearest_rides_for_location_from_and_to AS (
        SELECT ride_id, (ride_distance_from + ride_distance_to) AS distance
            FROM _nearest_rides_for_location_from_plus_ride_distance_to
    ),
    all_rides AS (
        SELECT id AS ride_id, 1 AS distance FROM rides
        WHERE (:status is null OR :status = status)
            AND (:type is null OR :type = type)
            AND datetime_start > :current_datetime
        LIMIT :limit OFFSET :offset
    ),

    ride_and_distance_only AS (
        SELECT ride_id, distance FROM :sub_query_placeholder
    )

SELECT :fields
    FROM ride_and_distance_only as found_ride
    LEFT JOIN rides AS ride ON found_ride.ride_id = ride.id
    INNER JOIN ride__locations AS location ON ride.id = location.ride_id
    LEFT JOIN profile ON ride.user_id = profile.user_id
    ORDER BY found_ride.distance, ride.datetime_start DESC;
