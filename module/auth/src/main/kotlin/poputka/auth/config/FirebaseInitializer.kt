package poputka.auth.config

import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class FirebaseInitializer(
        @Value("\${app.firebase.database}") database: String
) {
    init {
        val options = FirebaseOptions.Builder()
                .setCredentials(GoogleCredentials.getApplicationDefault())
                .setDatabaseUrl("https://$database.firebaseio.com/")
                .build()

        FirebaseApp.initializeApp(options)
    }
}