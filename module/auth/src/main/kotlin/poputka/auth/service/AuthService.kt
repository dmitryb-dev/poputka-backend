package poputka.auth.service

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import poputka.auth.domain.AppUser
import poputka.auth.service.entry.FirebaseEntryStore
import poputka.auth.service.user.UsersStore
import poputka.common.WithLogger
import poputka.common.exception.LocalizedException
import java.lang.IllegalArgumentException


@Service
class AuthService(
        private val usersStore: UsersStore,
        private val tokenManager: TokenManager,
        private val firebaseEntryStore: FirebaseEntryStore
) {
    @Transactional
    fun signInWithFirebase(firebaseToken: String): SignInResult {
        val firebaseUserId = try {
            FirebaseAuth.getInstance().verifyIdToken(firebaseToken).uid
        } catch (ex: FirebaseAuthException) {
            throw IncorrectCredentialsException()
        } catch (ex: IllegalArgumentException) {
            throw IncorrectCredentialsException()
        }

        val userId = firebaseEntryStore.getAppUserId(firebaseUserId) ?: try {
            log.info("App user for firebase user {} doesn't exist and is going to be created", firebaseUserId)

            val newUserId = usersStore.createNewUser()
            firebaseEntryStore.createEntry(firebaseUserId, newUserId)
            newUserId
        } catch (ex: FirebaseEntryStore.EntryAlreadyExistsException) {
            log.warn("Race condition found, user didn't exist before existence check but exists during new user creation")
            firebaseEntryStore.getAppUserId(firebaseUserId)!!
        }

        log.info("Firebase user {} logged in as {}", firebaseUserId, userId)
        return SignInResult(
                userId,
                tokenManager.createSignInToken(AppUser(userId))
        )
    }

    class SignInResult(val userId: Long, val token: String)

    class IncorrectCredentialsException : LocalizedException("Incorrect credentials", "incorrect_credentials")

    companion object : WithLogger
}