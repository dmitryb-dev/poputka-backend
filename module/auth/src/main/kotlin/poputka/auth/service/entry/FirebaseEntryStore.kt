package poputka.auth.service.entry

import poputka.auth.service.user.UserId

interface FirebaseEntryStore {
    fun getAppUserId(firebaseUserId: String): UserId?
    fun createEntry(firebaseUserId: String, appUserId: Long)

    class EntryAlreadyExistsException : Exception()
}