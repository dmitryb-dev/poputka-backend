package poputka.auth.service.user

typealias UserId = Long

interface UsersStore {
    fun createNewUser(): UserId
}