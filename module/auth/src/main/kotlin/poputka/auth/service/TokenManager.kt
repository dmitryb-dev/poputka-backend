package poputka.auth.service

import poputka.auth.domain.AppUser

typealias Token = String

interface TokenManager {
    fun loadUser(token: Token): AppUser?
    fun createSignInToken(user: AppUser): String
}
