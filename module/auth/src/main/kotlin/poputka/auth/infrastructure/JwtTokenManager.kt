package poputka.auth.infrastructure

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import poputka.auth.domain.AppUser
import poputka.auth.service.Token
import poputka.auth.service.TokenManager


@Component
class JwtTokenManager(
        @Value("\${app.auth.jwt.hs256.secret}") secret: String
) : TokenManager {

    private val jwtSignAlgorithm = Algorithm.HMAC256(secret)
    private val jwtVerifier = JWT
            .require(jwtSignAlgorithm)
            .withSubject("auth")
            .build()

    override fun loadUser(token: Token): AppUser? =
        try {
            val userId = jwtVerifier.verify(token).getClaim("userId").asLong()

            AppUser(userId)
        } catch (exception: Exception) {
            null
        }

    override fun createSignInToken(user: AppUser): String =
            JWT.create()
                .withSubject("auth")
                .withClaim("userId", user.id)
                .sign(jwtSignAlgorithm)
}
