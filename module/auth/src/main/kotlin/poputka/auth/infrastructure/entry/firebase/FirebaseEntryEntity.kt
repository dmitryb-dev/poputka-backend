package poputka.auth.infrastructure.entry.firebase

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "auth__firebase")
data class FirebaseEntryEntity (
        @Id
        @Column(name = "firebase_user_id")
        val firebaseUserId: String,

        @Column(name = "app_user_id")
        val appUserId: Long
)