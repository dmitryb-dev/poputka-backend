package poputka.auth.infrastructure.entry.firebase

import org.springframework.stereotype.Repository
import poputka.auth.service.entry.FirebaseEntryStore
import poputka.auth.service.user.UserId
import javax.persistence.EntityExistsException
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Repository
class JpaFirebaseEntryStore(
        @PersistenceContext private val entityManager: EntityManager
) : FirebaseEntryStore {
    override fun getAppUserId(firebaseUserId: String): UserId? {
        return entityManager.find(FirebaseEntryEntity::class.java, firebaseUserId)
                ?.appUserId
    }

    override fun createEntry(firebaseUserId: String, appUserId: Long) {
        try {
            entityManager.persist(FirebaseEntryEntity(firebaseUserId, appUserId))
        } catch (ex: EntityExistsException) {
            throw FirebaseEntryStore.EntryAlreadyExistsException()
        }
    }
}