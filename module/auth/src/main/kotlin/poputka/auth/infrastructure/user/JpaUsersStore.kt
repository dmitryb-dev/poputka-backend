package poputka.auth.infrastructure.user

import org.springframework.stereotype.Repository
import poputka.auth.service.user.UserId
import poputka.auth.service.user.UsersStore
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Repository
class JpaUsersStore(
        @PersistenceContext private val entityManager: EntityManager
) : UsersStore {
    override fun createNewUser(): UserId {
        val user = UserEntity(0L)
        entityManager.persist(user)
        return user.userId
    }
}