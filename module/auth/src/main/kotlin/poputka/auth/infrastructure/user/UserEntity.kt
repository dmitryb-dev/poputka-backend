package poputka.auth.infrastructure.user

import javax.persistence.*

@Entity
@Table(name = "auth__users")
data class UserEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "user_id")
        val userId: Long
)