package poputka.common

import org.slf4j.LoggerFactory

interface WithLogger {
    val log
        get() = LoggerFactory.getLogger(this.javaClass)
}