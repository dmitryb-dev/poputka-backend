package poputka.common.exception

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.stereotype.Component

open class LocalizedException(
        message: String?,
        private val code: String,
        private vararg val args: Any?
) : RuntimeException(message) {
    fun getLocalizedMessage(): String = lazy {
        MessageSourceHolder.messageSource
                .getMessage("exceptions.$code", args, LocaleContextHolder.getLocale())
    }.value
}

@Component
private class MessageSourceHolder{
    @Autowired
    private fun init(messageSource: MessageSource) {
        MessageSourceHolder.messageSourceProp = messageSource
    }

    companion object {
        private var messageSourceProp: MessageSource? = null

        val messageSource
            get() = messageSourceProp!!
    }
}