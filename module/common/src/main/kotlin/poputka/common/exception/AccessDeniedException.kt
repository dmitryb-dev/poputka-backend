package poputka.common.exception

class AccessDeniedException : LocalizedException("You don't have permissions for this action", "access_denied")