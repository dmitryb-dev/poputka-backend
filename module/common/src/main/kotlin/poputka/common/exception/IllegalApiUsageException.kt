package poputka.common.exception

class IllegalApiUsageException(message: String? = null) : LocalizedException(message, "illegal_usage")