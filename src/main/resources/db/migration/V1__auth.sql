CREATE TABLE auth__users(
    user_id bigserial primary key
);

CREATE TABLE auth__firebase(
    firebase_user_id varchar(256) primary key,
    app_user_id bigint
);