CREATE TABLE profile(
    user_id bigserial primary key,
    name varchar(256),
    surname varchar(256),
    image_url varchar(1024),
    phone varchar(64),
    email varchar(256),
    city varchar(256),
    country varchar(256),
    gender varchar(64)
);