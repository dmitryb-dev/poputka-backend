CREATE TABLE rides(
    id bigserial primary key,
    seats_total int,
    seats_taken int,
    date date,
    time_start time,
    time_end time,
    datetime_start timestamp,
    type varchar(64),
    status varchar(64),
    description varchar(2048),
    user_id bigint
);

CREATE TABLE ride__locations(
    id bigserial primary key,
    ride_id bigint references rides(id),
    point point
);

CREATE INDEX rides__type__index ON rides(status, type, datetime_start DESC);
CREATE INDEX rides__user_id__index ON rides(user_id);



-- imitates GENERATED ALWAYS AS, because it's not supported by IDEA now and spoils dev experience
CREATE FUNCTION rides__generate_datetime_start() RETURNS TRIGGER AS
$BODY$
BEGIN
    NEW.datetime_start = NEW.date + NEW.time_start;

    RETURN NEW;
END;
$BODY$
    language plpgsql;

CREATE TRIGGER ride__added
    BEFORE INSERT OR UPDATE ON rides
    FOR EACH ROW
EXECUTE PROCEDURE rides__generate_datetime_start();