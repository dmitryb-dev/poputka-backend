CREATE TABLE ride__search(
    location_id bigserial primary key,
    ride_id bigint,
    status varchar(64),
    type varchar(64),
    datetime_start timestamp,
    point point
);



CREATE INDEX ride__search__ride_id__index ON ride__search(ride_id);

CREATE INDEX ride__search__spatial__index ON ride__search USING gist(status, type, datetime_start, point);



CREATE FUNCTION ride__search__add_location() RETURNS TRIGGER AS
$BODY$
BEGIN
    INSERT INTO ride__search(location_id, ride_id, status, type, datetime_start, point)
        SELECT NEW.id as location_id, NEW.ride_id, r.status, r.type, r.date + r.time_start, NEW.point
            FROM rides as r
        WHERE r.id = NEW.ride_id;
    RETURN NEW;
END;
$BODY$
    language plpgsql;

CREATE TRIGGER ride__search__on_location_added
    AFTER INSERT ON ride__locations
    FOR EACH ROW
    EXECUTE PROCEDURE ride__search__add_location();



CREATE FUNCTION ride__search__delete_location() RETURNS TRIGGER AS
$BODY$
BEGIN
    DELETE FROM ride__search WHERE location_id = OLD.id;
    RETURN OLD;
END;
$BODY$
    language plpgsql;

CREATE TRIGGER ride__search__on_location_deleted
    AFTER DELETE ON ride__locations
    FOR EACH ROW
EXECUTE PROCEDURE ride__search__delete_location();



CREATE FUNCTION ride__search__update_ride() RETURNS TRIGGER AS
$BODY$
BEGIN
    UPDATE ride__search SET 
        status = NEW.status, 
        type = NEW.type, 
        datetime_start = NEW.date + NEW.time_start
    WHERE ride_id = NEW.id;
    RETURN NEW;
END;
$BODY$
    language plpgsql;

CREATE TRIGGER ride__search__on_rides_update
    AFTER UPDATE ON rides
    FOR EACH ROW
EXECUTE PROCEDURE ride__search__update_ride();