package poputka

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration
import org.springframework.boot.runApplication
import springfox.documentation.swagger2.annotations.EnableSwagger2

@EnableSwagger2
@SpringBootApplication(exclude = [ UserDetailsServiceAutoConfiguration::class ])
class PoputkaApplication

fun main(args: Array<String>) {
    runApplication<PoputkaApplication>(*args)
}
