package poputka.entrypoint.auth

import org.springframework.context.annotation.Profile
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import poputka.auth.domain.AppUser
import poputka.auth.service.Token
import poputka.auth.service.TokenManager

@Profile("dev")
@RequestMapping("api/dev/auth")
@RestController
class DevAuthController(
        private val tokenManager: TokenManager
) {

    @GetMapping("sign-in/{userId}")
    fun loginAsUser(
            @PathVariable userId: Long
    ): Token = tokenManager.createSignInToken(AppUser(userId))
}