package poputka.entrypoint.auth

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import poputka.auth.service.AuthService

@RequestMapping("api/auth")
@RestController
class AuthController(
        private val authService: AuthService
) {
    @PostMapping("sign-in/firebase")
    fun signInWithFirebase(
            @RequestBody request: SignInWithFirebaseRequest
    ): SignedInResponse {
        val signInResult = authService.signInWithFirebase(request.token)

        return SignedInResponse(signInResult.userId, signInResult.token)
    }

    class SignInWithFirebaseRequest(val token: String)
    class SignedInResponse(val userId: Long, val token: String)
}