package poputka.entrypoint

import org.springframework.context.MessageSource
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.http.HttpStatus
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.HttpMediaTypeNotSupportedException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import poputka.auth.service.AuthService
import poputka.common.WithLogger
import poputka.common.exception.LocalizedException

@RestControllerAdvice
class ExceptionHandler(
        private val messageSource: MessageSource
) : WithLogger {

    @ExceptionHandler(AuthService.IncorrectCredentialsException::class, AccessDeniedException::class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    fun onBadCredentials(exception: LocalizedException) = onLocalizedException(exception)

    @ExceptionHandler(NoSuchElementException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun onNotFound() = ErrorResponse(
            messageSource.getMessage("exceptions.not_found", null, LocaleContextHolder.getLocale()),
            "Requested resource was not found"
    )

    @ExceptionHandler(LocalizedException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun onLocalizedException(exception: LocalizedException) = ErrorResponse(
            exception.localizedMessage,
            exception.message ?: "no dev message available"
    )

    @ExceptionHandler(HttpMessageNotReadableException::class, HttpMediaTypeNotSupportedException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun devException(th: Throwable): ErrorResponse {
        return ErrorResponse(
                messageSource.getMessage("exceptions.illegal_usage", null, LocaleContextHolder.getLocale()),
                th.message ?: "Incorrect request"
        )
    }

    @ExceptionHandler(Throwable::class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    fun onServerError(th: Throwable): ErrorResponse {
        log.error("Unhandled exception", th)

        return ErrorResponse(
                messageSource.getMessage("exceptions.server_error", null, LocaleContextHolder.getLocale()),
                "Internal server error"
        )
    }

    class ErrorResponse(val localizedMessage: String, val devMessage: String)
}