package poputka.entrypoint.rides

import org.springframework.data.domain.PageRequest
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import poputka.auth.domain.AppUser
import poputka.common.exception.IllegalApiUsageException
import poputka.entrypoint.rides.dto.RideEntity
import poputka.entrypoint.rides.dto.RideType
import poputka.entrypoint.rides.dto.RideValue
import poputka.rides.service.RidesManager
import springfox.documentation.annotations.ApiIgnore
import java.time.LocalDateTime
import java.util.*

@RequestMapping("/api/rides")
@RestController
class RidesController(private val ridesManager: RidesManager) {

    @GetMapping
    fun findRides(
            @RequestParam(required = false) fromLatitude: Double?,
            @RequestParam(required = false) fromLongitude: Double?,

            @RequestParam(required = false) toLatitude: Double?,
            @RequestParam(required = false) toLongitude: Double?,

            @RequestParam(defaultValue = "20") radiusKm: Double?,

            @RequestParam(required = false) rideType: RideType?,

            @RequestParam(defaultValue = "0") pageNumber: Int,
            @RequestParam(defaultValue = "30") pageSize: Int,

            @ApiIgnore userTimezone: TimeZone
    ): RidesResponse {
        val rides = ridesManager.findRides(
                parseLocation(fromLatitude, fromLongitude),
                parseLocation(toLatitude, toLongitude),
                radiusKm,
                rideType?.toDomain(),
                LocalDateTime.now(userTimezone.toZoneId()),
                PageRequest.of(pageNumber, pageSize)
        )

        return RidesResponse(
                rides.map { RideEntity.fromDomain(it) }.toList()
        )
    }

    @GetMapping("/my")
    fun getUserRides(
            @AuthenticationPrincipal user: AppUser,
            @RequestParam(defaultValue = "0") pageNumber: Int,
            @RequestParam(defaultValue = "30") pageSize: Int
    ): RidesResponse {
        val rides = ridesManager.findUserRides(user.id, PageRequest.of(pageNumber, pageSize))

        return RidesResponse(
                rides.map { RideEntity.fromDomain(it) }.toList()
        )
    }

    @GetMapping("/{rideId}")
    fun getRide(@PathVariable rideId: Long): RideEntity {
        val ride = ridesManager.findById(rideId)

        return RideEntity.fromDomain(ride)
    }

    @PostMapping
    fun addRide(@AuthenticationPrincipal user: AppUser, @RequestBody request: RideValue): AddRideResponse {
        val rideId = ridesManager.addRide(user.id, request.toStoreDto())
        return AddRideResponse(rideId)
    }

    @PutMapping("{rideId}")
    fun updateRide(@AuthenticationPrincipal user: AppUser, @PathVariable rideId: Long, @RequestBody request: RideValue) {
        ridesManager.updateRide(user.id, rideId, request.toStoreDto())
    }

    private fun parseLocation(latitude: Double?, longitude: Double?) =
            if (latitude != null && longitude != null) {
                poputka.rides.domain.Location(latitude, longitude, null)
            } else if (latitude == null && longitude == null) {
                null
            } else throw IllegalApiUsageException("Both latitude and longitude must be provided or both omitted")

    class RidesResponse(val rides: Collection<RideEntity>)

    class AddRideResponse(val rideId: Long)
}