package poputka.entrypoint.rides.dto

import com.fasterxml.jackson.annotation.JsonFormat
import poputka.rides.domain.Ride
import poputka.rides.service.RidesManager
import java.time.LocalDate
import java.time.LocalTime

class RideValue(
        val locations: Collection<Location>,
        val seats: Seats,
        val type: RideType,
        val status: RideStatus,
        val departure: DepartureTime,
        val description: String
) {
    fun toStoreDto() = RidesManager.RideValue(
            poputka.rides.domain.DepartureTime(
                    departure.date,
                    departure.timeStart,
                    departure.timeEnd
            ),
            locations.map { poputka.rides.domain.Location(it.latitude, it.longitude, it.description) },
            poputka.rides.domain.Seats(seats.total, seats.taken),
            type.toDomain(),
            status.toDomain(),
            description
    )
}

class RideEntity(
        val id: Long,
        val user: User,
        val locations: Collection<Location>,
        val seats: Seats,
        val type: RideType,
        val status: RideStatus,
        val departure: DepartureTime,
        val description: String
) {
    companion object {
        fun fromDomain(ride: Ride) = RideEntity(
                ride.id,
                User(ride.user.userId, ride.user.name, ride.user.surname, ride.user.imageUrl),
                ride.locations.map { Location(it.latitude, it.longitude, it.description) },
                Seats(ride.seats.total, ride.seats.taken),
                RideType.fromDomain(ride.type),
                RideStatus.fromDomain(ride.status),
                DepartureTime(
                        ride.departureTime.date,
                        ride.departureTime.timeStart,
                        ride.departureTime.timeEnd
                ),
                ride.description
        )
    }
}

class Seats(val total: Int, val taken: Int)
class User(val id: Long, val name: String?, val surname: String?, val imageUrl: String?)
class Location(val latitude: Double, val longitude: Double, val description: String?)
class DepartureTime(
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
        val date: LocalDate,

        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
        val timeStart: LocalTime,

        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
        val timeEnd: LocalTime
)