package poputka.entrypoint.rides.dto

enum class RideType {
    DRIVER, PASSENGER;

    fun toDomain() = when (this) {
        DRIVER -> poputka.rides.domain.RideType.DRIVER
        PASSENGER -> poputka.rides.domain.RideType.PASSENGER
    }

    companion object {
        fun fromDomain(type: poputka.rides.domain.RideType) = when (type) {
            poputka.rides.domain.RideType.DRIVER -> DRIVER
            poputka.rides.domain.RideType.PASSENGER -> PASSENGER
        }
    }
}