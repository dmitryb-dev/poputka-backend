package poputka.entrypoint.rides.dto

enum class RideStatus {
    OPEN, CLOSED, FINISHED;

    fun toDomain() = when (this) {
        OPEN -> poputka.rides.domain.RideStatus.OPEN
        CLOSED -> poputka.rides.domain.RideStatus.CLOSED
        FINISHED -> poputka.rides.domain.RideStatus.FINISHED
    }

    companion object {
        fun fromDomain(status: poputka.rides.domain.RideStatus) = when (status) {
            poputka.rides.domain.RideStatus.OPEN -> OPEN
            poputka.rides.domain.RideStatus.CLOSED -> CLOSED
            poputka.rides.domain.RideStatus.FINISHED -> FINISHED
        }
    }
}