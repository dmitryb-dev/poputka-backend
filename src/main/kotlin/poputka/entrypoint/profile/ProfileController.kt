package poputka.entrypoint.profile

import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import poputka.auth.domain.AppUser
import poputka.common.exception.AccessDeniedException
import poputka.profile.service.ProfileStore

@RequestMapping("/api/profile")
@RestController
class ProfileController(
        private val profileStore: ProfileStore
) {

    @GetMapping
    fun findProfile(@AuthenticationPrincipal user: AppUser): Profile {
        return findProfile(user, user.id)
    }

    @GetMapping("{userId}")
    fun findProfile(@AuthenticationPrincipal user: AppUser, @PathVariable userId: Long): Profile {
        val found = profileStore.getProfile(userId)

        return Profile.fromDomain(found)
    }

    @PutMapping
    fun setProfile(@AuthenticationPrincipal user: AppUser, @RequestBody request: Profile) {
        if (request.userId != null && user.id != request.userId) {
            throw AccessDeniedException()
        }

        profileStore.saveProfile(request.toDomain(user.id))
    }


    class Profile(
            val userId: Long? = null,
            val name: String? = null,
            val surname: String? = null,
            val imageUrl: String? = null,
            val phone: String? = null,
            val email: String? = null,
            val city: String? = null,
            val country: String? = null,
            val gender: Gender? = null
    ) {
        fun toDomain(userId: Long) = poputka.profile.domain.Profile(
                userId,
                name,
                surname,
                imageUrl,
                phone,
                email,
                city,
                country,
                when (gender) {
                    Gender.MALE -> poputka.profile.domain.Gender.MALE
                    Gender.FEMALE -> poputka.profile.domain.Gender.FEMALE
                    null -> null
                }
        )

        companion object {
            fun fromDomain(profile: poputka.profile.domain.Profile?) = if (profile != null) {
                Profile(
                        profile.userId,
                        profile.name,
                        profile.surname,
                        profile.imageUrl,
                        profile.phone,
                        profile.email,
                        profile.city,
                        profile.country,
                        when (profile.gender) {
                            poputka.profile.domain.Gender.MALE -> Gender.MALE
                            poputka.profile.domain.Gender.FEMALE -> Gender.FEMALE
                            null -> null
                        }
                )
            } else {
                Profile()
            }
        }
    }

    enum class Gender {
        MALE, FEMALE
    }
}