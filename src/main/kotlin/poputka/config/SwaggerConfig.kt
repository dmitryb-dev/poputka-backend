package poputka.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.RestController
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiKey
import springfox.documentation.service.AuthorizationScope
import springfox.documentation.service.SecurityReference
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spi.service.contexts.SecurityContext
import springfox.documentation.spring.web.plugins.Docket


@Configuration
class SwaggerConfig {

    @Bean
    fun docs(): Docket = Docket(DocumentationType.SWAGGER_2)
            .ignoredParameterTypes(AuthenticationPrincipal::class.java)
            .securitySchemes(listOf(ApiKey("apikey", "access_token", "query")))
            .securityContexts(listOf(SecurityContext.builder()
                    .securityReferences(listOf(
                            SecurityReference(
                                    "apikey",
                                    arrayOf<AuthorizationScope>(AuthorizationScope("global", "accessEverything"))
                            )
                    ))
                    .forPaths(PathSelectors.regex("/.*"))
                    .build()
            ))
            .select()
            .apis(RequestHandlerSelectors.withClassAnnotation(RestController::class.java))
            .paths(PathSelectors.any())
            .build()
}
