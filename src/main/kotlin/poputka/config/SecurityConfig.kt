package poputka.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.factory.PasswordEncoderFactories
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import poputka.infrastrucutre.auth.JwtTokenAuthenticationProvider
import poputka.infrastrucutre.auth.JwtTokenExtractionFilter


@Configuration
class SecurityConfig(
        private val tokenAuthenticationProvider: JwtTokenAuthenticationProvider
) : WebSecurityConfigurerAdapter() {

    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.authenticationProvider(tokenAuthenticationProvider)
    }

    override fun configure(http: HttpSecurity) {
        http.addFilterBefore(JwtTokenExtractionFilter(this.authenticationManager()), BasicAuthenticationFilter::class.java)

        http.csrf().disable()

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)

        http.authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS).permitAll()
                .antMatchers("/api/dev/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/rides").permitAll()
                .antMatchers("/api/auth/sign-in/**").permitAll()
                .antMatchers("/api/**").authenticated()
                .anyRequest().permitAll()
    }

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder()
    }
}
