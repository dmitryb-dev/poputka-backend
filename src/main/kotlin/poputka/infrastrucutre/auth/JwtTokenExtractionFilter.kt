package poputka.infrastrucutre.auth

import org.springframework.http.HttpHeaders
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtTokenExtractionFilter(
        private val authenticationManager: AuthenticationManager
) : OncePerRequestFilter() {

    override fun doFilterInternal(
            request: HttpServletRequest,
            response: HttpServletResponse,
            filterChain: FilterChain
    ) {
        val token: String? = request.getParameter("access_token") ?: extractBearerToken(request)

        if (token != null) {
            try {
                val authResult = authenticationManager.authenticate(JwtToken(token))
                SecurityContextHolder.getContext().authentication = authResult
            } catch (e: AuthenticationException) {}
        }

        filterChain.doFilter(request, response)
    }

    private fun extractBearerToken(request: HttpServletRequest): String? =
            request.getHeader(HttpHeaders.AUTHORIZATION)
                ?.takeIf { header -> header.startsWith("Bearer ") }
                ?.substringAfter("Bearer ")

}