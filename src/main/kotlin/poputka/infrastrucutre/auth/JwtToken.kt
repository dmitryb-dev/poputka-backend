package poputka.infrastrucutre.auth

import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import poputka.auth.domain.AppUser


data class JwtToken @JvmOverloads constructor(
        private val tokenString: String,
        private var principal: AppUser? = null
) : AbstractAuthenticationToken(listOf(SimpleGrantedAuthority("ROLE_USER"))) {
    override fun getCredentials(): String = tokenString

    override fun getPrincipal(): AppUser? = this.principal
}
