package poputka.infrastrucutre.auth

import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component
import poputka.auth.service.TokenManager

@Component
class JwtTokenAuthenticationProvider(
        private val tokenManager: TokenManager
) : AuthenticationProvider {

    override fun authenticate(authentication: Authentication): Authentication {
        val token = authentication as JwtToken

        val user = tokenManager.loadUser(token.credentials) ?: throw BadCredentialsException("Incorrect sign in token")

        return token.copy(principal = user)
    }

    override fun supports(authentication: Class<*>): Boolean {
        return JwtToken::class.java.isAssignableFrom(authentication)
    }

}
