rootProject.name = "poputka"

include("module:common")
include("module:auth")
include("module:rides")
include("module:profile")